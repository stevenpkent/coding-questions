(ns philip.core-test
  (:require [clojure.test :refer :all]
            [philip.core :refer :all]))

(deftest tic-tac-toe
  (is (= "x wins" (check [[:x :o :x] [:x :o :o] [:x :x :o]])))
  (is (= "o wins" (check [[:o :x :x] [:x :o :x] [:x :o :o]])))
  (is (= "no winner" (check [[:x :o :x] [:x :o :x] [:o :x :o]]))))

(deftest android-pattern-match
  (is (false? (valid-path? [1 9])))
  (is (true?  (valid-path? [2 1 3])))
  (is (false? (valid-path? [1 3 2])))
  (is (false? (valid-path? [3 1 2])))
  (is (true?  (valid-path? [4 1 7])))
  (is (false? (valid-path? [1 7 4])))
  (is (false? (valid-path? [7 1 4])))
  (is (true?  (valid-path? [5 1 9])))
  (is (false? (valid-path? [1 9 5])))
  (is (false? (valid-path? [9 1 5])))
  (is (true?  (valid-path? [1 5 9 8 2])))
  (is (true?  (valid-path? [1 6 7 4])))
  (is (true?  (valid-path? [2 1 3])))
  (is (false? (valid-path? [1 3 2])))
  (is (false? (valid-path? [1 9])))
  (is (false? (valid-path? [1 2 3 2 1])))
  (is (false? (valid-path? [0 1 2 3])))
  (is (true?  (valid-path? [1 2 3 4 5 6 7 8 9])))
  (is (true?  (valid-path? [9 8 7 6 5 4 3 2 1])))
  (is (true?  (valid-path? [5 1 9 2 4 7 3])))
  (is (true?  (valid-path? [5 4 6 2 8]))))

(deftest count-word-in-matrix
  (is (= 2 (count-words-in-matrix matrix "HELLO")))
  (is (= 1 (count-words-in-matrix matrix "WORLD")))
  (is (= 2 (count-words-in-matrix matrix "BUZZ")))
  (is (= 0 (count-words-in-matrix matrix "CLOJURE")))
  (is (= 0 (count-words-in-matrix matrix "COWABUNGA"))))

(deftest _two-sum
  (is (= [1 5] (two-sum 6 [9 2 8 7 1 4])))
  (is (= [2 5] (two-sum 12 [9 2 8 7 1 4])))
  (is (= "NO COMBO FOUND" (two-sum 99 [1 4 5])))
  (is (= "NO COMBO FOUND" (two-sum 6 [3 9 8 4 5]))))

(deftest _two-sum-2
  (is (= [1 4] (two-sum-2 6 [3 2 8 1 4])))
  (is (= [1 5] (two-sum-2 6 [9 2 8 7 1 4])))
  (is (= [2 5] (two-sum-2 12 [9 2 8 7 1 4])))
  (is (= "NO COMBO FOUND" (two-sum-2 99 [1 4 5])))
  (is (= "NO COMBO FOUND" (two-sum-2 6 [3 9 8 4 5]))))

