(ns philip.core
  (:gen-class))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tic-tac-toe result checker

(defn ttt
  [board]
  (let [horizontals board
        verticals (apply mapv vector board)
        [[a _ b]
         [_ c _]
         [d _ e]] board
        diagonals [[a c e] [b c d]]]
    (->> (concat horizontals verticals diagonals)
         (map set)
         (filter #(= (count %) 1))
         (ffirst))))

(defn check
  [board]
  (condp = (ttt board)
    nil "no winner"
    :x "x wins"
    :o "o wins"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; android matrix pattern checker

(defn which-row
  [n]
  (cond
    (= (#{1 2 3} n) n) 1
    (= (#{4 5 6} n) n) 2
    (= (#{7 8 9} n) n) 3))

(defn row-difference
  [f l]
  (Math/abs (- (which-row f) (which-row l))))

(defn find-midpoint
  [f l]
  (/ (+ f l) 2))

(defn midpoint-occurs-earlier?
  [midpoint f l coll]
  (let [idx-f (.indexOf coll f)
        idx-m (.indexOf coll midpoint)
        idx-l (.indexOf coll l)]
    (and (not= idx-m -1)
         (< idx-m idx-f)
         (< idx-m idx-l))))

(defn validate
  [diff subset coll]
  (let [f (first subset) l (last subset) m (find-midpoint f l)]
    (condp = diff
      2 (if (= (row-difference f l) 0)
          (midpoint-occurs-earlier? m f l coll)
          true)
      4 (if (= (row-difference f l) 2)
          (midpoint-occurs-earlier? m f l coll)
          true)
      (midpoint-occurs-earlier? m f l coll))))

(defn handle-even-diffs
  [even-diffs coll]
  (if (empty? even-diffs)
    true
    (->> (for [e-d even-diffs] (validate (Math/abs (- (first e-d) (last e-d))) e-d coll))
         (every? true?))))

(defn find-even-diffs
  [coll]
  (for [[x y] (partition 2 1 coll)
        :when (even? (Math/abs (- x y)))]
    [x y]))

(defn valid-path?
  [coll]
  (and
   (= (count (set coll)) (count coll))
   (every? #(and (>= % 1) (<= % 9)) coll)
   (handle-even-diffs (find-even-diffs coll) coll)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word frequency

(def matrix ["AOTDLROW"
             "LCBMUMLU"
             "DRUJDBLJ"
             "PAZHZZEF"
             "BCZELFHW"
             "RKULVPPG"
             "ALBLPOPQ"
             "BEMOPPJY"])

(defn includes
  [s matrix]
  (->> (filter #(clojure.string/includes? % s) matrix)
       count))

(defn includes-reverse
  [s matrix]
  (->> (map clojure.string/reverse matrix)
       (includes s)))

(defn count-words-in-matrix
  [matrix s]
  (let [vertical-matrix (vec (for [mat (apply mapv vector matrix)] (apply str mat)))]
    (+
     (includes s matrix)
     (includes s vertical-matrix)
     (includes-reverse s matrix)
     (includes-reverse s vertical-matrix))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; two sum - using loop/recur

(defn two-sum
  [target coll]
  (let [cnt (dec (count coll))]
    (loop [slow-iterator 0 fast-iterator 0]
      (if (and (not= slow-iterator fast-iterator)
               (= (+ (coll slow-iterator) (coll fast-iterator)) target))
        [slow-iterator fast-iterator]
        (cond
          (and (= slow-iterator cnt) (= fast-iterator cnt)) "NO COMBO FOUND"
          (and (not= slow-iterator cnt) (= fast-iterator cnt)) (recur (inc slow-iterator) 0)
          :else (recur slow-iterator (inc fast-iterator)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; two sum - not using loop/recur
;; work in progress - I'm sure this can be simplified

(defn sum-helper
  [idx coll rng]
  (apply assoc {} (interleave rng (map #(+ (coll idx) (coll %1)) rng))))

(defn filter-coll
  [target rng coll]
  (let [src (map #(flatten (map vals (vals (nth coll %)))) rng)]
    (->>
     (for [each-map-item src] (some #{target} each-map-item))
     (map #(= (nil? %) false))
     (vec)
     (keep-indexed #(when (true? %2) %1))
     (map #(nth coll %)))))

(defn filter-coll-2
  [target coll]
  (let [rng (range 0 (count coll))]
    [(flatten (map keys coll))
     (map #(.indexOf (flatten (map vals (vals (nth coll %)))) target) rng)]))

(defn solve
  [[x y]]
  (let [thingy (partition 2 (interleave x y))
        rng (range 0 (count thingy))
        which (first (filter #(not= (first (nth thingy %)) (last (nth thingy %))) rng))]
    (if (nil? which)
      "NO COMBO FOUND"
      (vec (nth thingy which)))))

(defn two-sum-2
  [target coll]
  (let [rng (range 0 (count coll))]
    (->>
     (map #(hash-map %1 (sum-helper %1 coll rng)) rng)
     (filter-coll target rng)
     (filter-coll-2 target)
     (solve))))

